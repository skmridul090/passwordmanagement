package Models;

import java.io.Serializable;

public class PasswordEncoderHashTable implements Serializable{
	private String password;
	private String hashCode;
	private String passwordType;
	
	private static PasswordEncoderHashTable encoder;
	
	public static PasswordEncoderHashTable Encrypt() {
		if (encoder == null) {
			encoder = new PasswordEncoderHashTable();
		}
		return encoder;
	}
	
	public PasswordEncoderHashTable addRealAndEncryptedPassword(PasswordEncoderHashTable encrypt, String password, String hashcode, String passwordType) {
		encrypt.password = password;
		encrypt.hashCode = hashcode;
		encrypt.passwordType = passwordType;
		
		return encrypt;
	}

	public String getPassword() {
		return password;
	}

	public String getHashCode() {
		return hashCode;
	}

	public String getPasswordType() {
		return passwordType;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public void setPasswordType(String passwordType) {
		this.passwordType = passwordType;
	}
	
	
	
}
