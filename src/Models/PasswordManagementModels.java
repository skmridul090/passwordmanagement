package Models;

import java.io.Serializable;

import Account.UserClass;

public class PasswordManagementModels implements Serializable{
	
	private String username;

	private String password;

	private String PasswordType;

	private String websiteName;

	private String websiteURL;

	private String applicationName;

	private String gameName;

	private String gameDeveloper;

	private String dateCreated;

	private String dateLastUpdated;

	private UserClass User;
	
	private static PasswordManagementModels passmanage;
	
	public static PasswordManagementModels getPasswordModel() {
		if(passmanage == null) {
			passmanage = new PasswordManagementModels();
		}
		return passmanage;
	}
	
	public PasswordManagementModels addWebsitePassword(PasswordManagementModels passManag, String username, String password, String PasswordType, String websiteName, String websiteURL, String dateCreated, String dateLastUpdated, UserClass User) {
		passManag.username = username;
		passManag.password = password;
		passManag.PasswordType = PasswordType;
		passManag.websiteName = websiteName;
		passManag.websiteURL = websiteURL;
		passManag.dateCreated = dateCreated;
		passManag.dateLastUpdated = dateLastUpdated;
		passManag.User = User;
		
		return passManag;
	}
	
	public PasswordManagementModels addDesktopApplicationPassword(PasswordManagementModels passManag, String username, String password, String PasswordType, String applicationName, String dateCreated, String dateLastUpdated, UserClass User) {
		passManag.username = username;
		passManag.password = password;
		passManag.PasswordType = PasswordType;
		passManag.applicationName = applicationName;
		passManag.dateCreated = dateCreated;
		passManag.dateLastUpdated = dateLastUpdated;
		passManag.User = User;
		
		return passManag;
	}
	
	
	public PasswordManagementModels addGamePassword(PasswordManagementModels passManag, String username, String password, String PasswordType, String gameName, String gameDeveloper, String dateCreated, String dateLastUpdated, UserClass User) {
		passManag.username = username;
		passManag.password = password;
		passManag.PasswordType = PasswordType;
		passManag.gameName = gameName;
		passManag.gameDeveloper = gameDeveloper;
		passManag.dateCreated = dateCreated;
		passManag.dateLastUpdated = dateLastUpdated;
		passManag.User = User;
		
		return passManag;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordType() {
		return PasswordType;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public String getGameName() {
		return gameName;
	}

	public String getGameDeveloper() {
		return gameDeveloper;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public String getDateLastUpdated() {
		return dateLastUpdated;
	}

	public UserClass getUser() {
		return User;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public void setGameDeveloper(String gameDeveloper) {
		this.gameDeveloper = gameDeveloper;
	}

	public void setDateLastUpdated(String dateLastUpdated) {
		this.dateLastUpdated = dateLastUpdated;
	}
	
	

	
	
}
