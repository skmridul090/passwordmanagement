package PasswordManager;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

import Account.UserClass;
import Models.PasswordManagementModels;
import Models.PasswordEncoderHashTable;

import java.io.*;
import java.security.*;
import java.util.Base64;
import java.util.Random;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;




public class ManagePassword {
	public String choise="continue";
	public int opptionChosen = 0;
	ArrayList<PasswordManagementModels> passwordList = new ArrayList<>();
	ArrayList<PasswordEncoderHashTable> EncrypetedPasswordList = new ArrayList<>();
	Scanner scan = new Scanner(System.in);
	
	PasswordManagementModels passMangModels;
	PasswordEncoderHashTable encryptPassword;
	
	// Format the date and time using a specific pattern (optional)
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	LocalDateTime currentDateTime = LocalDateTime.now();
	 
	String Date;
	String UpdateDate;

    // Secret key for AES encryption (make sure to keep it secure)
    private static final String secretKey = "ThisIsAValidKey1";
	
	
	public ManagePassword(UserClass user) {
		//load all already stored data.
		passwordList = loadAllPassword();
		
		System.out.print("\t\t Password Manager System \n");
		while(choise=="continue") {
			if(user.getUserType().equals("user")) {
				System.out.print("Please chose an opption \n");
				System.out.print("1. Do you want to add a password \n");
				System.out.print("4. Do you want to view all the password \n");
				System.out.print("5. Do you want to search the password \n");
				System.out.print("6. Do you want Exit \n");
			}
			else {
				System.out.print("Please chose an opption \n");
				System.out.print("1. Do you want to add a password \n");
				System.out.print("2. Do you want to update a password \n");
				System.out.print("3. Do you want to delete a password  \n");
				System.out.print("4. Do you want to view all the password \n");
				System.out.print("5. Do you want to search the password \n");
				System.out.print("6. Do you want Exit \n");
			}
			
			opptionChosen = scan.nextInt();
			scan.nextLine();
			
	        switch (opptionChosen) {
	        	case 1:
	        		addPassword(user);
	        		break;
	        	case 2:
	        		updatePassword(passwordList);
	        		break;
	        	case 3:
	        		deletePassword(passwordList);
	        		break;
	        	case 4:
	        		for(PasswordManagementModels pasmanag: passwordList) {
	        			
	        			if (pasmanag.getUser().getUserName().equals(user.getUserName()) || user.getUserType().equals("admin")) {
	        				
	        				if(pasmanag.getPasswordType().equals("Website")) {
	        					System.out.println("name: "+pasmanag.getUsername()+" password: "+pasmanag.getPassword()+" website name: "+pasmanag.getWebsiteName()+" website URL: "+pasmanag.getWebsiteURL()+" Date of Created: "+pasmanag.getDateCreated()+" Date of Update: "+pasmanag.getDateLastUpdated());
	        				}
	        				else if(pasmanag.getPasswordType().equals("Desktop")) {
	        					System.out.println("name: "+pasmanag.getUsername()+" password: "+pasmanag.getPassword()+" Desktop Application Name: "+pasmanag.getApplicationName()+" Date of Created: "+pasmanag.getDateCreated()+" Date of Update: "+pasmanag.getDateLastUpdated());
	        				}
	        				else {
	        					System.out.println("name: "+pasmanag.getUsername()+" password: "+pasmanag.getPassword()+" Game Name: "+pasmanag.getGameName()+" Game Developer: "+pasmanag.getGameDeveloper()+" Date of Created: "+pasmanag.getDateCreated()+" Date of Update: "+pasmanag.getDateLastUpdated());
	        				}
	        			}
	        			
	        			
	        		}
	        		break;
	        	case 5:
	        		System.out.print("Please write down the name of website/application/game! \n");
	        		String search = scan.nextLine();
	        		for(PasswordManagementModels pasmanag: passwordList) {
	        			        			
	        			if ( (pasmanag.getWebsiteName() != null) && (pasmanag.getWebsiteName().equals(search)) ) {
	        				System.out.println("name: "+pasmanag.getUsername()+" password: "+pasmanag.getPassword()+" website name: "+pasmanag.getWebsiteName()+" website URL: "+pasmanag.getWebsiteURL()+" Date of Created: "+pasmanag.getDateCreated()+" Date of Update: "+pasmanag.getDateLastUpdated());
	        			}
	        			else if( (pasmanag.getApplicationName() != null) && (pasmanag.getApplicationName().equals(search)) ) {
	        				System.out.println("name: "+pasmanag.getUsername()+" password: "+pasmanag.getPassword()+" Desktop Application Name: "+pasmanag.getApplicationName()+" Date of Created: "+pasmanag.getDateCreated()+" Date of Update: "+pasmanag.getDateLastUpdated());
	        			}
	        			else if( (pasmanag.getGameName() != null) && (pasmanag.getGameName().equals(search)) ) {
	        				System.out.println("name: "+pasmanag.getUsername()+" password: "+pasmanag.getPassword()+" Game Name: "+pasmanag.getGameName()+" Game Developer: "+pasmanag.getGameDeveloper()+" Date of Created: "+pasmanag.getDateCreated()+" Date of Update: "+pasmanag.getDateLastUpdated());
	        			}
	        			
	        		}
	        		
	        		
	        		break;
	        	case 6:
	    			choise="exit";
	        		break;	        		
	        	default:
	        		System.out.println("Invalid day");
	        }
			
		}	
		
	}
	
	public void addPassword(UserClass user) {
	    EncrypetedPasswordList = LoadEncryptedPassword();
	    System.out.print("Please chose an Options \n");
	    System.out.print("1. Do you want to add Website credentials \n");
	    System.out.print("2. Do you want to add Desktop application credentials \n");
	    System.out.print("3. Do you want to add Game's credentials  \n");

	    opptionChosen = scan.nextInt();
	    scan.nextLine();
	    int passwordOption;
	    String username;
        String realPassword;
     // Encrypt the password
        String encryptedPassword;
		
		switch (opptionChosen) {
    	case 1:
    		passMangModels = PasswordManagementModels.getPasswordModel();
    		// Create a new instance of PasswordEncoderHashTable
            encryptPassword = PasswordEncoderHashTable.Encrypt();
            
            Date = currentDateTime.format(formatter);
            UpdateDate = currentDateTime.format(formatter);
            
            System.out.println("Do you want to enter your own password or generate a random one?");
            System.out.println("1. Enter my own password");
            System.out.println("2. Generate a random password");
            
            passwordOption = scan.nextInt();
            scan.nextLine(); // Consume the newline character
           
            
            if (passwordOption == 1) {
                System.out.println("Please provide name, password, website name, website URL");
                username = scan.nextLine();
                realPassword = scan.nextLine();
            } else if (passwordOption == 2) {
            	System.out.println("Please provide name");
            	username = scan.nextLine();
            	realPassword = generateRandomPassword();
                System.out.println("Generated password: " + realPassword);
                
            } else {
                System.out.println("Invalid option. Exiting...");
                return;
            }

            // Encrypt the password
            encryptedPassword = encrypt(realPassword);
            System.out.println(" the encrypted password is " + encryptedPassword);
            System.out.println("Please also provide website name, website URL");
            passMangModels = passMangModels.addWebsitePassword(passMangModels, username, encryptedPassword, "Website",
                    scan.nextLine(), scan.nextLine(), Date, UpdateDate, user);
            
         // Use the instance to add real and encrypted password
            encryptPassword = encryptPassword.addRealAndEncryptedPassword(encryptPassword, realPassword, encryptedPassword, "Website");
                        
            passwordList.add(passMangModels);
            EncrypetedPasswordList.add(encryptPassword);

            // Save the encrypted password along with real password and hash code in encryptedfile.txt
            saveEncryptedPassword(EncrypetedPasswordList);

            saveAllPassword(passwordList);
            System.out.println(" save successfully");
            break;
            
    	case 2:
    		passMangModels = PasswordManagementModels.getPasswordModel();
    		// Create a new instance of PasswordEncoderHashTable
            encryptPassword = PasswordEncoderHashTable.Encrypt();
            
            Date = currentDateTime.format(formatter);
            UpdateDate = currentDateTime.format(formatter);
            
            System.out.println("Do you want to enter your own password or generate a random one?");
            System.out.println("1. Enter my own password");
            System.out.println("2. Generate a random password");
            
            passwordOption = scan.nextInt();
            scan.nextLine(); // Consume the newline character
      
            if (passwordOption == 1) {
                System.out.println("please provide name, password, Desktop Application Name");
                username = scan.nextLine();
                realPassword = scan.nextLine();
            } else if (passwordOption == 2) {
            	System.out.println("Please provide name");
            	username = scan.nextLine();
            	realPassword = generateRandomPassword();
                System.out.println("Generated password: " + realPassword);
                
            } else {
                System.out.println("Invalid option. Exiting...");
                return;
            }

            // Encrypt the password
            encryptedPassword = encrypt(realPassword);
            System.out.println(" the encrypted password is " + encryptedPassword);
            System.out.println("Please also provide Desktop Application Name");
            passMangModels = passMangModels.addDesktopApplicationPassword(passMangModels, username, encryptedPassword, "Desktop",
                    scan.nextLine(), Date, UpdateDate, user);
            
         // Use the instance to add real and encrypted password
            encryptPassword = encryptPassword.addRealAndEncryptedPassword(encryptPassword, realPassword, encryptedPassword, "Desktop");
                        
            passwordList.add(passMangModels);
            EncrypetedPasswordList.add(encryptPassword);

            // Save the encrypted password along with real password and hash code in encryptedfile.txt
            saveEncryptedPassword(EncrypetedPasswordList);

            saveAllPassword(passwordList);
            System.out.println(" save successfully");
            break;
    	case 3:
       		passMangModels = PasswordManagementModels.getPasswordModel();
    		// Create a new instance of PasswordEncoderHashTable
            encryptPassword = PasswordEncoderHashTable.Encrypt();
            
            Date = currentDateTime.format(formatter);
            UpdateDate = currentDateTime.format(formatter);
            
            System.out.println("Do you want to enter your own password or generate a random one?");
            System.out.println("1. Enter my own password");
            System.out.println("2. Generate a random password");
            
            passwordOption = scan.nextInt();
            scan.nextLine(); // Consume the newline character
           
            
            if (passwordOption == 1) {
                System.out.println("Please provide name, password, Game name, Game Developer");
                username = scan.nextLine();
                realPassword = scan.nextLine();
            } else if (passwordOption == 2) {
            	System.out.println("Please provide name");
            	username = scan.nextLine();
            	realPassword = generateRandomPassword();
                System.out.println("Generated password: " + realPassword);
                
            } else {
                System.out.println("Invalid option. Exiting...");
                return;
            }

            // Encrypt the password
            encryptedPassword = encrypt(realPassword);
            System.out.println(" the encrypted password is " + encryptedPassword);
            System.out.println("Please also provide Game name, Game Developer");
            passMangModels = passMangModels.addWebsitePassword(passMangModels, username, encryptedPassword, "Game",
                    scan.nextLine(), scan.nextLine(), Date, UpdateDate, user);
            
         // Use the instance to add real and encrypted password
            encryptPassword = encryptPassword.addRealAndEncryptedPassword(encryptPassword, realPassword, encryptedPassword, "Game");
                        
            passwordList.add(passMangModels);
            EncrypetedPasswordList.add(encryptPassword);

            // Save the encrypted password along with real password and hash code in encryptedfile.txt
            saveEncryptedPassword(EncrypetedPasswordList);

            saveAllPassword(passwordList);
            System.out.println(" save successfully");
            break;
    		
    		
    	default:
    		System.out.println("Invalid day");
		}
		
		
	}
	
	
    // Encrypt the password using AES
    private String encrypt(String password) {
        try {
            if (password == null || password.isEmpty()) {
                throw new IllegalArgumentException("Password cannot be null or empty");
            }

            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] encryptedBytes = cipher.doFinal(password.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    
    // Decrypt the password using AES
    private String decrypt(String encryptedPassword) {
        try {
            if (encryptedPassword == null || encryptedPassword.isEmpty()) {
                throw new IllegalArgumentException("Encrypted password cannot be null or empty");
            }

            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedPassword));
            return new String(decryptedBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private String generateRandomPassword() {
        // Define the criteria for generating a random password
        String uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String specialCharacters = "!@#$%^&*()-_=+[]{}|;:'<>,.?/";

        // Combine all possible characters
        String allCharacters = uppercaseLetters + lowercaseLetters + numbers + specialCharacters;

        // Set the desired length of the password
        int passwordLength = 12;

        // Generate a random password
        StringBuilder generatedPassword = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < passwordLength; i++) {
            int randomIndex = random.nextInt(allCharacters.length());
            generatedPassword.append(allCharacters.charAt(randomIndex));
        }

        return generatedPassword.toString();
    }
	
	

	
	public ArrayList<PasswordManagementModels> loadAllPassword() {
		try {
			FileInputStream getExistingFileDataIn = new FileInputStream("ManagePasswords.txt");
			ObjectInputStream getExistingObjectDataIn = new ObjectInputStream(getExistingFileDataIn);
			
			passwordList = (ArrayList<PasswordManagementModels>)getExistingObjectDataIn.readObject();
			
			getExistingObjectDataIn.close();
			getExistingFileDataIn.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
			if(e2.getMessage() != "null") {
				System.out.println(e2.getMessage());
			}
			
		}
		
		return passwordList;

	}
	
	public void updatePassword(ArrayList<PasswordManagementModels> passList) {
	    System.out.println(" Please select the type of password you want to update");
	    System.out.println(" 1. Website");
	    System.out.println(" 2. Application");
	    System.out.println(" 3. Game");

	    String nameOfTheUpdatedPassword;
	    opptionChosen = scan.nextInt();
	    scan.nextLine();

	    if (opptionChosen == 1) {
	        System.out.println("Please write down the website name you want to update");
	        nameOfTheUpdatedPassword = scan.nextLine();

	        for (int i = 0; i < passwordList.size(); i++) {
	            PasswordManagementModels pasmanag = passwordList.get(i);
	            
	            if ( (pasmanag.getWebsiteName() != null) && (pasmanag.getWebsiteName().equals(nameOfTheUpdatedPassword)) ) {
	                System.out.println("Currently stored details are,");
	                System.out.println("name: " + pasmanag.getUsername() + " password: " + pasmanag.getPassword()
	                        + " website name: " + pasmanag.getWebsiteName() + " website URL: " + pasmanag.getWebsiteURL()
	                        + " Date of Created: " + pasmanag.getDateCreated() + " Date of Update: "
	                        + pasmanag.getDateLastUpdated());

	                // Update details
	                System.out.println("Please input new details,");
	                System.out.print("New username: ");
	                pasmanag.setUsername(scan.nextLine());
	                System.out.print("New password: ");
	                pasmanag.setPassword(scan.nextLine());
	                System.out.print("New website name: ");
	                pasmanag.setWebsiteName(scan.nextLine());
	                System.out.print("New website URL: ");
	                pasmanag.setWebsiteURL(scan.nextLine());
	                pasmanag.setDateLastUpdated(currentDateTime.format(formatter));

	                // Replace the existing object in the list with the updated one
	                passwordList.set(i, pasmanag);

	                // Save the updated list to the text file
	                saveAllPassword(passwordList);

	                System.out.println("Password updated successfully.");
	            }
	        }
	    } else if (opptionChosen == 2 ) {
	    	System.out.println("Please write down the Desktop Application name you want to update");
	        nameOfTheUpdatedPassword = scan.nextLine();

	        for (int i = 0; i < passwordList.size(); i++) {
	            PasswordManagementModels pasmanag = passwordList.get(i);
	           
	            if ( (pasmanag.getApplicationName() != null) && (pasmanag.getApplicationName().equals(nameOfTheUpdatedPassword)) ) {
	                System.out.println("Currently stored details are,");
	                System.out.println("name: " + pasmanag.getUsername() + " password: " + pasmanag.getPassword()
	                        + " Application name: " + pasmanag.getApplicationName() + " Date of Created: " + pasmanag.getDateCreated() + 
	                        " Date of Update: "+ pasmanag.getDateLastUpdated());

	                // Update details
	                System.out.println("Please input new details,");
	                System.out.print("New username: ");
	                pasmanag.setUsername(scan.nextLine());
	                System.out.print("New password: ");
	                pasmanag.setPassword(scan.nextLine());
	                System.out.print("New Application name: ");
	                pasmanag.setApplicationName(scan.nextLine());
	                pasmanag.setDateLastUpdated(currentDateTime.format(formatter));

	                // Replace the existing object in the list with the updated one
	                passwordList.set(i, pasmanag);

	                // Save the updated list to the text file
	                saveAllPassword(passwordList);

	                System.out.println("Password updated successfully.");
	            }
	        }
	    }
	    else if(opptionChosen == 3) {
	        System.out.println("Please write down the Games name you want to update");
	        nameOfTheUpdatedPassword = scan.nextLine();

	        for (int i = 0; i < passwordList.size(); i++) {
	            PasswordManagementModels pasmanag = passwordList.get(i);
	            
	            if ( (pasmanag.getGameName() != null) && (pasmanag.getGameName().equals(nameOfTheUpdatedPassword)) ) {
	                System.out.println("Currently stored details are,");
	                System.out.println("name: " + pasmanag.getUsername() + " password: " + pasmanag.getPassword()
	                        + " Games name: " + pasmanag.getGameName() + " Developer: " + pasmanag.getGameDeveloper()
	                        + " Date of Created: " + pasmanag.getDateCreated() + " Date of Update: "
	                        + pasmanag.getDateLastUpdated());

	                // Update details
	                System.out.println("Please input new details,");
	                System.out.print("New username: ");
	                pasmanag.setUsername(scan.nextLine());
	                System.out.print("New password: ");
	                pasmanag.setPassword(scan.nextLine());
	                System.out.print("New Game name: ");
	                pasmanag.setGameName(scan.nextLine());
	                System.out.print("New Game Develper: ");
	                pasmanag.setGameDeveloper(scan.nextLine());
	                pasmanag.setDateLastUpdated(currentDateTime.format(formatter));

	                // Replace the existing object in the list with the updated one
	                passwordList.set(i, pasmanag);

	                // Save the updated list to the text file
	                saveAllPassword(passwordList);

	                System.out.println("Password updated successfully.");
	            }
	        }
	    }
	    else {
	        System.out.println("Invalid option");
	    }
	}
	
	public void deletePassword(ArrayList<PasswordManagementModels> passList) {
	    System.out.println("Please write down the name of website/application/game to delete");
	    String nameToDelete = scan.nextLine();

	    boolean passwordFound = false;

	    for (int i = 0; i < passList.size(); i++) {
	        PasswordManagementModels pasmanag = passList.get(i);

	        if ((pasmanag.getWebsiteName() != null) && pasmanag.getWebsiteName().equals(nameToDelete)
	                && pasmanag.getPasswordType().equals("Website")) {
	        	
	            System.out.println("Details of the password to be deleted are,");
	            System.out.println("name: " + pasmanag.getUsername() + " password: " + pasmanag.getPassword()
	                    + " website name: " + pasmanag.getWebsiteName() + " website URL: " + pasmanag.getWebsiteURL()
	                    + " Date of Created: " + pasmanag.getDateCreated() + " Date of Update: "
	                    + pasmanag.getDateLastUpdated());

	            System.out.println("Are you sure you want to delete this password? (yes/no)");
	            String confirmation = scan.nextLine();

	            if (confirmation.equalsIgnoreCase("yes")) {
	                // Remove the password from the list
	                passwordList.remove(i);

	                // Save the updated list to the text file
	                saveAllPassword(passwordList);

	                System.out.println("Password deleted successfully.");
	            } else {
	                System.out.println("Deletion cancelled.");
	            }

	            passwordFound = true;
	            break; // Exit the loop after deleting the password
	        } else if ((pasmanag.getApplicationName() != null) && pasmanag.getApplicationName().equals(nameToDelete)
	                && pasmanag.getPasswordType().equals("Desktop")) {
	            
	        	System.out.println("Details of the password to be deleted are,");
	            System.out.println("name: " + pasmanag.getUsername() + " password: " + pasmanag.getPassword()
	                    + " Desktop Application: " + pasmanag.getApplicationName()
	                    + " Date of Created: " + pasmanag.getDateCreated() + " Date of Update: "
	                    + pasmanag.getDateLastUpdated());

	            System.out.println("Are you sure you want to delete this password? (yes/no)");
	            String confirmation = scan.nextLine();

	            if (confirmation.equalsIgnoreCase("yes")) {
	                // Remove the password from the list
	                passwordList.remove(i);

	                // Save the updated list to the text file
	                saveAllPassword(passwordList);

	                System.out.println("Password deleted successfully.");
	            } else {
	                System.out.println("Deletion cancelled.");
	            }

	            passwordFound = true;
	            break; // Exit the loop after deleting the password

	        } else if ((pasmanag.getGameName() != null) && pasmanag.getGameName().equals(nameToDelete)
	                && pasmanag.getPasswordType().equals("Game")) {

	        	System.out.println("Details of the password to be deleted are,");
	            System.out.println("name: " + pasmanag.getUsername() + " password: " + pasmanag.getPassword()
	                    + " game name: " + pasmanag.getGameName() + " Game Developer: " + pasmanag.getGameDeveloper()
	                    + " Date of Created: " + pasmanag.getDateCreated() + " Date of Update: "
	                    + pasmanag.getDateLastUpdated());

	            System.out.println("Are you sure you want to delete this password? (yes/no)");
	            String confirmation = scan.nextLine();

	            if (confirmation.equalsIgnoreCase("yes")) {
	                // Remove the password from the list
	                passwordList.remove(i);

	                // Save the updated list to the text file
	                saveAllPassword(passwordList);

	                System.out.println("Password deleted successfully.");
	            } else {
	                System.out.println("Deletion cancelled.");
	            }

	            passwordFound = true;
	            break; // Exit the loop after deleting the password
	        	
	        }
	    }

	    // Print a message if the password was not found
	    if (!passwordFound) {
	        System.out.println("Password not found.");
	    }
	}
	
    // load the real password, encrypted password, and hash code in encryptedfile.txt
    private ArrayList<PasswordEncoderHashTable> LoadEncryptedPassword() {
        
		try {
			FileInputStream readRealPassword = new FileInputStream("encryptedfile.txt");
			ObjectInputStream getExistingRealPasswordIn = new ObjectInputStream(readRealPassword);
			
			EncrypetedPasswordList = (ArrayList<PasswordEncoderHashTable>)getExistingRealPasswordIn.readObject();
			
			getExistingRealPasswordIn.close();
			readRealPassword.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
			if(e2.getMessage() != "null") {
				System.out.println(e2.getMessage());
			}
			
		}
		return EncrypetedPasswordList;
		
    }
    
    // Save the real password, encrypted password, and hash code in encryptedfile.txt
    private void saveEncryptedPassword(ArrayList<PasswordEncoderHashTable> enryPassList) {
        try {
			FileOutputStream saveEncryptedPassword = new FileOutputStream("encryptedfile.txt");
			ObjectOutputStream saveEncryptedPasswordObjectDataOut = new ObjectOutputStream(saveEncryptedPassword);
			
			saveEncryptedPasswordObjectDataOut.writeObject(enryPassList);
			saveEncryptedPasswordObjectDataOut.flush();
			saveEncryptedPassword.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
			System.out.println(e2.getMessage());
		}
    }
	
	
	public void saveAllPassword(ArrayList<PasswordManagementModels> passList ) {
		// write this newly added data to file 
        try {
			FileOutputStream getExistingFileDataOut = new FileOutputStream("ManagePasswords.txt");
			ObjectOutputStream getExistingObjectDataOut = new ObjectOutputStream(getExistingFileDataOut);
			
			getExistingObjectDataOut.writeObject(passList);
			getExistingObjectDataOut.flush();
			getExistingFileDataOut.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
			System.out.println(e2.getMessage());
		}
	}
}