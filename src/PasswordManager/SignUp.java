package PasswordManager;

//importing classes from other package
import Account.UserClass;
import BinarryTree.BinarySearchTree;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//import the Java library classes
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SignUp {
	private static SignUp register = null;
	
	ArrayList<UserClass> users = new ArrayList<>();
	BinarySearchTree bst = new BinarySearchTree(); 
	LogIn lgin;
	
	//Necessary object declaration
	Scanner scan = new Scanner(System.in);
	private String userName;
	private String userEmail;
	private String userPassword;
	private String userType;
	private int choseOpptions = 0;
	
	public SignUp() {}
	
	public static SignUp getSignUp() {
		if (register == null) {
			register = new SignUp();
		}
		return register; 
	}
	
	public void registerUser() {
		//load all the previous user's details and insert it the binary tree for checking duplication insert
		bst = loadAllUser(bst);
		System.out.print("Is it 1.Admin user or 2.Normal User ");
		choseOpptions = scan.nextInt();
		scan.nextLine(); // Consume the newline character
		if(choseOpptions == 1) {
			System.out.println("\nPlease input Admin User Name, Email, Password");
			userName = scan.nextLine();
			userEmail = scan.nextLine();
			userPassword = scan.next();
			addNewUser(bst, userName, userEmail, userPassword, "admin");
			
			//After Signup auto login
			lgin = LogIn.getlogin();
			lgin.autoLogInAfterSignUp(userName, userPassword);
		}
		else if(choseOpptions == 2) {
			System.out.println("\nPlease input Normal User Name, Email, Password");	
			userName = scan.nextLine();
			userEmail = scan.nextLine();
			userPassword = scan.next();
			addNewUser(bst, userName, userEmail, userPassword, "user");
			
			//After Signup auto login
			lgin = LogIn.getlogin();
			lgin.autoLogInAfterSignUp(userName, userPassword);
		}
	}
	
	private BinarySearchTree loadAllUser(BinarySearchTree bst) {
		try {
			FileInputStream getExistingFileDataIn = new FileInputStream("UserClass.txt");
			ObjectInputStream getExistingObjectDataIn = new ObjectInputStream(getExistingFileDataIn);
			
			users = (ArrayList<UserClass>)getExistingObjectDataIn.readObject();
			
			getExistingObjectDataIn.close();
			getExistingFileDataIn.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
//			System.out.println(e2.getMessage());
		}
		
		if (!users.isEmpty()) {
			for(UserClass usr: users) {
				UserClass existingUser = bst.search(usr.getUserName(), usr.getUserEmail());
				if (existingUser == null) {
					bst.insert(usr);
				}
			}
		}
		
		return bst;
	}
	
	private void addNewUser(BinarySearchTree bst, String name, String email, String password,String userType) {
		UserClass newUser = UserClass.getUser(name, email, password, userType);
		UserClass existingUser = bst.search(name, email);
		
        if (existingUser == null) {
            bst.insert(newUser);
            
            saveUserDetails(newUser);
            
            System.out.println("User added: " + newUser);
        }
        else {
        	System.out.println("This user is already exist");
        }

    }
	
	private void saveUserDetails(UserClass newUser) {
		//Add the existing data from the 'UserClass.txt' file to the Arraylist
		

		// Now update the arraylist and add the newly signup user details to our 'UserClass.txt' file
		users.add(newUser);
		
		// write this newly added data to file 
        try {
			FileOutputStream getExistingFileDataOut = new FileOutputStream("UserClass.txt");
			ObjectOutputStream getExistingObjectDataOut = new ObjectOutputStream(getExistingFileDataOut);
			
			getExistingObjectDataOut.writeObject(users);
			getExistingObjectDataOut.flush();
			getExistingFileDataOut.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
			System.out.println(e2.getMessage());
		}
		
		
	}
}
