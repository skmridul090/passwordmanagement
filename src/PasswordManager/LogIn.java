package PasswordManager;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Scanner;

import Account.UserClass;
import BinarryTree.BinarySearchTree;

public class LogIn {
	private static LogIn lgin;
	
	ArrayList<UserClass> users = new ArrayList<>();
	BinarySearchTree bst = new BinarySearchTree();
	Scanner scan = new Scanner(System.in);
	
	private String Name;
	private String Password;
	private boolean isEqual = false;
	ManagePassword mngPass;

	
	public static LogIn getlogin() {
		if(lgin == null) {
			lgin = new LogIn(); 
		}
		return lgin;
	}
	
	public void logInNow() {
		bst = loadAllUser(bst);
		System.out.println("Please provide user Name/Email and Password");
		try {
		Name = scan.nextLine();
		Password = scan.nextLine();
		UserClass existingUser = bst.searchForLogIn(Name, Name);
		
		if (existingUser != null) {
			isEqual = existingUser.getUserPassword().equals(Password);
			if(isEqual) {
				System.out.println("LogIn Successfull");
				mngPass = new ManagePassword(existingUser);
			}
			else {
				System.out.println("Password not match");
			}
			
		}
		else if(existingUser == null) {
			System.out.println("Couldn't find this user");
		}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void autoLogInAfterSignUp(String Name, String Password) {
		try {
			bst = loadAllUser(bst);
			UserClass existingUser = bst.searchForLogIn(Name, Name);
			
			if (existingUser != null) {
				isEqual = existingUser.getUserPassword().equals(Password);
				if(isEqual) {
					System.out.println("LogIn Successfull");
					mngPass = new ManagePassword(existingUser);
				}
				else {
					System.out.println("Password not match");
				}
				
			}
			else if(existingUser == null) {
				System.out.println("Couldn't find this user");
			}
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	private BinarySearchTree loadAllUser(BinarySearchTree bst) {
		try {
			FileInputStream getExistingFileDataIn = new FileInputStream("UserClass.txt");
			ObjectInputStream getExistingObjectDataIn = new ObjectInputStream(getExistingFileDataIn);
			
			users = (ArrayList<UserClass>)getExistingObjectDataIn.readObject();
			
			getExistingObjectDataIn.close();
			getExistingFileDataIn.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
		}
		
		if (!users.isEmpty()) {
			for(UserClass usr: users) {
				UserClass existingUser = bst.search(usr.getUserName(), usr.getUserEmail());
				if (existingUser == null) {
					bst.insert(usr);
				}
			}
		}
		return bst;
	}
}
