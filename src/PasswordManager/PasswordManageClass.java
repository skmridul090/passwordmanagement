package PasswordManager;

//importing classes from other package
import Account.UserClass;
import BinarryTree.BinarySearchTree;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//import the Java library classes
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Main operational class for our password manager
public class PasswordManageClass { 
		
	public static void main(String[] args) { 
		// TODO Auto-generated method stub

		 SignUp register;
		 LogIn lgin;
		 
		 //Necessary variable declaration
		 String choice = "Continue";
		 int choseOpptions = 0;
		 
		//Necessary object declaration
		 Scanner scan = new Scanner(System.in);
		 
		 try {
			 UserClass user = null;
			 System.out.println("\t\t\t @@Welcome to password management@@");	
			 System.out.println("Chose an Opption,\n1. SignUp \n2. LogIn");	
		 	choseOpptions = scan.nextInt();
		 
			 if(choseOpptions == 1) {
				 register = SignUp.getSignUp();
				 register.registerUser();
			 }
			 else if(choseOpptions == 2) {
				 lgin = LogIn.getlogin();
				 lgin.logInNow();
			 }
			 else {
				 System.out.println("Please choose a valid option!");	
			 }
		 }catch(Exception e) {
			 System.out.println(e.getMessage());
		 }
		 
		 showSavedData();

	}
	
	
	private static void showSavedData() {
		ArrayList<UserClass> users = new ArrayList<>();
		try {
			FileInputStream getExistingFileDataIn = new FileInputStream("UserClass.txt");
			ObjectInputStream getExistingObjectDataIn = new ObjectInputStream(getExistingFileDataIn);
			
			users = (ArrayList<UserClass>)getExistingObjectDataIn.readObject();
			
			getExistingObjectDataIn.close();
			getExistingFileDataIn.close();
			
			
			
		} catch (Exception e2) {
			// TODO: handle exception
			System.out.println(e2.getMessage());
		}
		for(UserClass usr: users) {
			System.out.println(usr.getUserName()+' '+usr.getUserEmail()+' '+usr.getUserPassword()+' '+usr.getUserType());
			}
	}
	

}
