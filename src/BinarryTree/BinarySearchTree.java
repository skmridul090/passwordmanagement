package BinarryTree;

import Account.UserClass;


public class BinarySearchTree { 
    private Node root;

    private static class Node {
        private UserClass user;
        private Node left;
        private Node right;

        public Node(UserClass user) {
            this.user = user;
            this.left = null;
            this.right = null;
        }
    }

    public void insert(UserClass user) {
        root = insertNode(root, user);
    }

    private Node insertNode(Node root, UserClass user) {
        if (root == null) {
            return new Node(user);
        }

        int compare = user.getUserName().compareTo(root.user.getUserName());

        if (compare < 0) {
            root.left = insertNode(root.left, user);
        } else if (compare > 0) {
            root.right = insertNode(root.right, user);
        } else {
            // User with the same name already exists
            System.out.println("Warning!! User with name '" + user.getUserName() + "' already exists.");
        }

        return root;
    }

    public UserClass search(String name, String email) {
        return searchNode(root, name, email);
    }

    private UserClass searchNode(Node root, String name, String email) {
        if (root == null) {
            return null; // User not found
        }

        int compareName = name.compareTo(root.user.getUserName());
        int compareEmail = email.compareTo(root.user.getUserEmail());

        if (compareName == 0 && compareEmail == 0) {
            return root.user; // User found
        } else if (compareName < 0) {
            return searchNode(root.left, name, email);
        } else {
            return searchNode(root.right, name, email);
        }
    }
    
    public UserClass searchForLogIn(String name, String email) {
  
        return searchNodeForLogIN(root, name, email); 
    }

    private UserClass searchNodeForLogIN(Node root, String name, String email) {
        if (root == null) {
            return null; // User not found
        }
        
        int compareName = name.compareTo(root.user.getUserName());
        int compareEmail = email.compareTo(root.user.getUserEmail());

        if (compareName == 0 || compareEmail == 0) {
            return root.user; // User found
        } else if (compareName < 0) {
            return searchNodeForLogIN(root.left, name, email);
        } else {
            return searchNodeForLogIN(root.right, name, email);
        }
    }
    
    //For print all connected users name on this binary tree
    public void printAllUserNames() {
        inorderTraversal(root);
    }

    private void inorderTraversal(Node root) {
        if (root != null) {
            inorderTraversal(root.left);
            System.out.println("User Name: " + root.user.getUserName()+" User Email: " + root.user.getUserEmail()+" User Password: " + root.user.getUserPassword());
            inorderTraversal(root.right);
        }
    }
}

