package Account;

import java.io.Serializable;

import BinarryTree.BinarySearchTree;

public class UserClass implements Serializable{
	// Private static instance of the class
	private static UserClass user;
	
	private String userName;
	private String userEmail;
	private String userPassword;
	private String userType;
	
	// Private constructor to prevent instantiation from outside the class
	private UserClass() {
	}
	
	//based on singleton coding pattern,  
	// provide public method to provide a global point of access to the instance  
	public static UserClass getUser(String userName,String userEmail,String userPassword,String userType) {
       
		// Lazy initialisation: create the instance only if it doesn't exist yet 
        if (user == null) {
        	user = new UserClass(); 
        	
        }
        user.userName = userName;
        user.userEmail = userEmail;
        user.userPassword = userPassword;
        user.userType = userType;
        
		return user;
	}

	//all the getter methods for class attribute
	public String getUserName() {
		return userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public String getUserType() {
		return userType;
	}
	
}
